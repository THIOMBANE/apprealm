package com.ghosthio.apprealm


import io.realm.RealmObject
import io.realm.annotations.RealmClass

@RealmClass

open class Dog : RealmObject() {

    var name:String = ""
    var age:Int = 0

    fun addYear(years:Int) {
        age += 1
    }
}